#include <ITAAudiofileWriter.h>
#include <ITAFFT.h>
#include <ITAFFTUtils.h>
#include <ITAFileSystemUtils.h>
#include <cassert>

void ITAFFTUtils::Convert( const ITABase::CHDFTSpectrum* pSpectrum, ITASampleBuffer& sbIR )
{
	sbIR.Init( pSpectrum->GetDFTSize( ) - 1, true );
	ITASampleBuffer sbSpectrum( pSpectrum->GetDFTSize( ), true ); // local copy

	ITAFFT ifft( ITAFFT::IFFT_C2R, sbIR.length( ), sbSpectrum.GetData( ), sbIR.GetData( ) );

	// Make local copy of input (will be destroyed by in-place FFT)
	sbSpectrum.write( pSpectrum->GetData( ), sbSpectrum.length( ) );
	float* fIn  = sbSpectrum.GetData( );
	float* fOut = sbIR.GetData( );
	ifft.execute( fIn, fOut );

	// Normalize after IFFT
	sbIR.div_scalar( float( sbIR.length( ) ) );

	return;
}

void ITAFFTUtils::Convert( const ITABase::CHDFTSpectra* pSpectra, ITASampleFrame& sfIR )
{
	sfIR.Init( pSpectra->GetNumChannels( ), pSpectra->GetDFTSize( ) - 1, true );
	ITASampleBuffer sbSourceData( pSpectra->GetDFTSize( ), true ); // local copy

	ITAFFT ifft( ITAFFT::IFFT_C2R, sfIR.GetLength( ), sbSourceData.GetData( ), sfIR[0].GetData( ) );

	for( int i = 0; i < pSpectra->GetNumChannels( ); i++ )
	{
		// Make local copy of input (will be destroyed by in-place FFT)
		sbSourceData.write( ( *pSpectra )[i]->GetData( ), sbSourceData.GetLength( ) );
		float* fIn  = sbSourceData.GetData( );
		float* fOut = sfIR[i].GetData( );
		ifft.execute( fIn, fOut );
	}

	// Normalize after IFFT
	sfIR.div_scalar( float( sfIR.length( ) ) );

	return;
}

void ITAFFTUtils::Convert( const ITABase::CFiniteImpulseResponse& oIR, ITABase::CHDFTSpectrum* pSpectrum )
{
	assert( pSpectrum->GetSampleRate( ) == oIR.GetSampleRate( ) );
	ITASampleBuffer sbSourceData( pSpectrum->GetDFTSize( ) - 1, true ); // local copy
	ITAFFT fft( ITAFFT::FFT_R2C, oIR.GetLength( ), sbSourceData.GetData( ), pSpectrum->GetData( ) );

	// Make local copy of input (will be destroyed by in-place FFT)
	sbSourceData.write( pSpectrum->GetData( ), sbSourceData.GetLength( ) );
	float* fIn  = sbSourceData.GetData( );
	float* fOut = pSpectrum->GetData( );
	fft.execute( fIn, fOut );

	return;
}

void ITAFFTUtils::Convert( const ITABase::CMultichannelFiniteImpulseResponse& oIR, ITABase::CHDFTSpectra* pSpectra )
{
	assert( pSpectra->GetSampleRate( ) == oIR.GetSampleRate( ) );
	ITASampleBuffer sbSourceData( pSpectra->GetDFTSize( ) - 1, true ); // local copy
	ITAFFT fft( ITAFFT::FFT_R2C, oIR.GetLength( ), sbSourceData.GetData( ), ( *pSpectra )[0]->GetData( ) );

	for( int n = 0; n < pSpectra->GetNumChannels( ); n++ )
	{
		auto pSpectrum = ( *pSpectra )[n];
		// Make local copy of input (will be destroyed by in-place FFT)
		sbSourceData.write( pSpectrum->GetData( ), sbSourceData.GetLength( ) );
		float* fIn  = sbSourceData.GetData( );
		float* fOut = pSpectrum->GetData( );
		fft.execute( fIn, fOut );
	}

	return;
}

void ITAFFTUtils::Export( const ITABase::CHDFTSpectrum* pSpectrum, const std::string& sFilePath, bool bNormalize /*= false */ )
{
	std::string sFilePathComplete = sFilePath;
	if( getFilenameSuffix( sFilePath ).empty( ) )
		sFilePathComplete += ".wav";
	std::string sFilePathFinal = correctPath( sFilePathComplete );

	ITASampleBuffer sbIR;
	ITAFFTUtils::Convert( pSpectrum, sbIR );
	if( bNormalize )
		sbIR.Normalize( );

	writeAudiofile( sFilePathFinal, &sbIR, pSpectrum->GetSampleRate( ), ITAQuantization::ITA_FLOAT );

	return;
}

void ITAFFTUtils::Export( const ITABase::CHDFTSpectra* pSpectra, const std::string& sFilePath, bool bNormalize /*= false */ )
{
	std::string sFilePathComplete = sFilePath;
	if( getFilenameSuffix( sFilePath ).empty( ) )
		sFilePathComplete += ".wav";
	std::string sFilePathFinal = correctPath( sFilePathComplete );

	ITASampleFrame sfIR;
	ITAFFTUtils::Convert( pSpectra, sfIR );
	if( bNormalize )
		sfIR.normalize( );

	const double dSampleRate = pSpectra->GetSampleRate( );
	writeAudiofile( sFilePathFinal, &sfIR, dSampleRate, ITAQuantization::ITA_FLOAT );

	return;
}
