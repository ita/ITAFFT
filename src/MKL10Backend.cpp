/*
 *  ITAFFT, eine Wrapper-Bibliothek für schnelle Fouriertransformationen
 *
 *  Autor: Frank Wefers (Frank.Wefers@akustik.rwth-aachen.de)
 *
 *  (c) Copyright Institut für Technische Akustik (ITA), RWTH Aachen
 *
 */

// $Id: MKL10Backend.cpp,v 1.2 2009-12-14 14:01:28 fwefers Exp $

#ifdef WITH_MKL10

#	include <ITAException.h>
#	include <ITAFFT.h>
#	include <MKL10Backend.h>

MKL10Realization::MKL10Realization( int type, int size, float* in, float* out ) : m_plan( NULL ), m_type( type ), m_size( size ), m_out( out )
{
	if( ( size <= 0 ) || ( in == NULL ) || ( out == NULL ) )
		ITA_EXCEPT0( INVALID_PARAMETER );

	fftw_iodim dim, howmany_dim;

	switch( type )
	{
		case ITAFFT::FFT_R2C:
			m_plan  = fftwf_plan_dft_r2c_1d( size, in, (fftwf_complex*)out, FFTW_ESTIMATE );
			m_sInfo = "FFT_R2C [MKL10]";
			break;

		case ITAFFT::FFT_C2C:
			m_plan  = fftwf_plan_dft_1d( size, (fftwf_complex*)in, (fftwf_complex*)out, FFTW_FORWARD, FFTW_ESTIMATE );
			m_sInfo = "FFT_C2C [MKL10]";
			break;

		case ITAFFT::IFFT_C2R:
			m_plan  = fftwf_plan_dft_c2r_1d( size, (fftwf_complex*)in, out, FFTW_ESTIMATE );
			m_sInfo = "IFFT_C2R [MKL10]";
			break;

		case ITAFFT::IFFT_C2C:
			m_plan  = fftwf_plan_dft_1d( size, (fftwf_complex*)in, (fftwf_complex*)out, FFTW_BACKWARD, FFTW_ESTIMATE );
			m_sInfo = "IFFT_C2C [MKL10]";
			break;

		case ITAFFT::SPLIT_FFT_R2C:
			dim.n  = size;
			dim.is = dim.os = 1;

			howmany_dim.n  = 1;
			howmany_dim.is = howmany_dim.os = 1;

			m_plan  = fftwf_plan_guru_split_dft_r2c( 1, &dim, 1, &howmany_dim, in, out, out + ( size / 2 ) + 1, FFTW_ESTIMATE );
			m_sInfo = "SPLIT_FFT_R2C [MKL10]";
			break;

		case ITAFFT::SPLIT_IFFT_C2R:
			dim.n  = size;
			dim.is = dim.os = 1;

			howmany_dim.n  = 1;
			howmany_dim.is = howmany_dim.os = 1;

			m_plan  = fftwf_plan_guru_split_dft_c2r( 1, &dim, 1, &howmany_dim, in, in + ( size / 2 ) + 1, out, FFTW_ESTIMATE );
			m_sInfo = "SPLIT_IFFT_C2R [MKL10]";
			break;

		case ITAFFT::NORMALIZED_SPLIT_IFFT_C2R:
			dim.n  = size;
			dim.is = dim.os = 1;

			howmany_dim.n  = 1;
			howmany_dim.is = howmany_dim.os = 1;

			m_plan  = fftwf_plan_guru_split_dft_c2r( 1, &dim, 1, &howmany_dim, in, in + ( size / 2 ) + 1, out, FFTW_ESTIMATE );
			m_sInfo = "NORMALIZED_SPLIT_IFFT_C2R [MKL10]";
			break;
	}

	if( m_plan == NULL )
		ITA_EXCEPT0( UNKNOWN );
}

MKL10Realization::~MKL10Realization( )
{
	if( m_plan != NULL )
		fftwf_destroy_plan( m_plan );
}

void MKL10Realization::execute( )
{
	fftwf_execute( m_plan );

	// Sonderfall NORMALIZED_SPLIT_IFFT_C2R: Hier noch die Normalisierung durchführen
	if( m_type == ITAFFT::NORMALIZED_SPLIT_IFFT_C2R )
		for( int i = 0; i < m_size; i++ )
			m_out[i] /= (float)m_size;
}

void MKL10Realization::execute( float* in, float* out )
{
	switch( m_type )
	{
		case ITAFFT::FFT_R2C:
			fftwf_execute_dft_r2c( m_plan, in, (fftwf_complex*)out );
			break;

		case ITAFFT::IFFT_C2R:
			fftwf_execute_dft_c2r( m_plan, (fftwf_complex*)in, out );
			break;

		case ITAFFT::FFT_C2C:
		case ITAFFT::IFFT_C2C:
			fftwf_execute_dft( m_plan, (fftwf_complex*)in, (fftwf_complex*)out );
			break;

		case ITAFFT::SPLIT_FFT_R2C:
			fftwf_execute_split_dft_r2c( m_plan, in, out, ( out + ( m_size / 2 ) + 1 ) );
			break;

		case ITAFFT::SPLIT_IFFT_C2R:
			fftwf_execute_split_dft_c2r( m_plan, in, ( in + ( m_size / 2 ) + 1 ), out );
			break;

		case ITAFFT::NORMALIZED_SPLIT_IFFT_C2R:
			fftwf_execute_split_dft_c2r( m_plan, in, ( in + ( m_size / 2 ) + 1 ), out );

			// Normalisierung durchführen
			for( int i = 0; i < m_size; i++ )
				out[i] /= (float)m_size;

			break;
	}
}

std::string MKL10Realization::toString( )
{
	return m_sInfo;
}

// -----------------------------------

MKL10Backend* MKL10Backend::m_pInstance = NULL;

MKL10Backend* MKL10Backend::getInstance( )
{
	if( m_pInstance == NULL )
		m_pInstance = new MKL10Backend( );
	return m_pInstance;
}

ITAFFTRealization* MKL10Backend::plan( int type, int size, float* in, float* out )
{
	return new MKL10Realization( type, size, in, out );
}

#endif WITH_MKL10
