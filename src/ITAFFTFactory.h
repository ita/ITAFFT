/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_FFT_FACTORY
#define INCLUDE_WATCHER_ITA_FFT_FACTORY

// Vorwärtsdeklarationen
class ITAFFT;

/* Diese abstrakte Klasse definiert die Schnittstelle für die Erzeugung
 * von Instanzen der Klasse ITAFFT. Sie wird von den Backends implementiert.
 */

class ITAFFTFactory
{
public:
	virtual ~ITAFFTFactory( ) { };

	virtual ITAFFT* plan_dft( float* in, float* out, int size, int flags )      = 0;
	virtual ITAFFT* plan_idft( float* in, float* out, int size, int flags )     = 0;
	virtual ITAFFT* plan_dft_r2c( float* in, float* out, int size, int flags )  = 0;
	virtual ITAFFT* plan_idft_c2r( float* in, float* out, int size, int flags ) = 0;
};

#endif // INCLUDE_WATCHER_ITA_FFT_FACTORY