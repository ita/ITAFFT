/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_FFTW3_BACKEND
#define INCLUDE_WATCHER_FFTW3_BACKEND

#include "ITAFFTBackend.h"
#include "ITAFFTRealization.h"

#include <fftw3.h>

#ifdef WIN32
#	include <windows.h>
#endif

class FFTW3Realization : public ITAFFTRealization
{
public:
	FFTW3Realization( int type, int size, float* in, float* out, unsigned int uiFlag = FFTW_MEASURE );
	~FFTW3Realization( );

	void execute( );
	void execute( float* in, float* out );

	std::string toString( );

private:
	fftwf_plan m_plan;
	int m_type;
	int m_size;
	bool m_inplace;
	float* m_out; // Ausgabepuffer (Wird nur f�r den Sonderfall NORMALIZED_SPLIT_IFFT_C2R bei execute() ben�tigt)
	std::string m_sInfo;

#ifdef WIN32
	static CRITICAL_SECTION m_csPlannerLock; // Kritischer Bereich um reentrant calls f�r die Planungsroutinen zu verhindern (FFTW Problem!)
	static volatile bool m_bPlannerLockInit; // Kritischen Bereich initialisiert?
#endif
};

class FFTW3Backend : public ITAFFTBackend
{
public:
	static FFTW3Backend* getInstance( );

	ITAFFTRealization* plan( int type, int size, float* in, float* out, unsigned int uiFlags );

private:
	static FFTW3Backend* m_pInstance; // Singleton-Instanz
};

#endif // INCLUDE_WATCHER_FFTW3_BACKEND
