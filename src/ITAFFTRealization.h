/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_FFT_REALIZATION
#define INCLUDE_WATCHER_ITA_FFT_REALIZATION

#include <string>

// Vorwärtsdeklarationen
class ITAFFTRealization;

/* Diese abstrakte Klasse definiert die Bibliotheks-intern benutzte Schnittstelle
 * für Realisierungen der FFT, welche von den verschiedenen Backends bereitgestellt werden.
 *
 * Die Methoden und ihre Bedeutungen gleichen denen mit gleichem Namen in der
 * öffentlichen Klasse ITAFFT.
 */

class ITAFFTRealization
{
public:
	virtual ~ITAFFTRealization( ) {}

	virtual void execute( )                       = 0;
	virtual void execute( float* in, float* out ) = 0;

	virtual std::string toString( ) = 0;
};

#endif // INCLUDE_WATCHER_ITA_FFT_REALIZATION
