/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_MKL10_BACKEND
#define IW_MKL10_BACKEND

#ifdef ITAFFT_WITH_FFTW3

// Hinweis: Diese Implementierung nutzt derzeit das FFTW3-Interface der MKL

#	include <ITAFFTBackend.h>
#	include <ITAFFTRealization.h>
#	include <fftw/fftw3.h>

class MKL10Realization : public ITAFFTRealization
{
public:
	MKL10Realization( int type, int size, float* in, float* out );
	~MKL10Realization( );

	void execute( );
	void execute( float* in, float* out );

	std::string toString( );

private:
	fftwf_plan m_plan;
	int m_type;
	int m_size;
	float* m_out; // Ausgabepuffer (Wird nur f�r den Sonderfall NORMALIZED_SPLIT_IFFT_C2R bei execute() ben�tigt)
	std::string m_sInfo;
};

class MKL10Backend : public ITAFFTBackend
{
public:
	static MKL10Backend* getInstance( );

	ITAFFTRealization* plan( int type, int size, float* in, float* out );

private:
	static MKL10Backend* m_pInstance; // Singleton-Instanz
};

#endif // ITAFFT_WITH_FFTW3

#endif // IW_MKL10_BACKEND
