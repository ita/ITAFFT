#include "ITAFFTRealization.h"

#include <ITAException.h>
#include <ITAFFT.h>

#ifdef ITAFFT_WITH_FFTW3
#	include "FFTW3Backend.h"
#else
#	define FFTW_MEASURE  0
#	define FFTW_ESTIMATE 1 << 6
#endif

#ifdef ITAFFT_WITH_MKL10
#	include "MKL10Backend.h"
#endif


ITAFFT::ITAFFT( ) : m_pRealization( NULL ) {}

ITAFFT::ITAFFT( int type, int size, float* in, float* out, int iPlannungMethod ) : m_pRealization( NULL )
{
	plan( type, size, in, out, iPlannungMethod );
}

ITAFFT::~ITAFFT( )
{
	delete m_pRealization;
}

bool ITAFFT::isPlanned( )
{
	return ( m_pRealization != NULL );
}

void ITAFFT::plan( int type, int size, float* in, float* out, int iPlannungMethod )
{
	// Falls bereits geplant wurde, wird der alte Plan verworfen
	delete m_pRealization;

	unsigned int uiFlags = 0;
	if( iPlannungMethod == PLAN_USING_MEASUREMENT )
		uiFlags |= FFTW_MEASURE;
	if( iPlannungMethod == PLAN_USING_ESTIMATION )
		uiFlags |= FFTW_ESTIMATE;

#ifdef ITAFFT_WITH_FFTW3
	m_pRealization = FFTW3Backend::getInstance( )->plan( type, size, in, out, uiFlags );
#endif

#ifdef ITAFFT_WITH_MKL10
	m_pRealization = MKL10Backend::getInstance( )->plan( type, size, in, out, uiFlags );
#endif

	if( m_pRealization == NULL )
		ITA_EXCEPT1( UNKNOWN, "Planning of the FFT failed" );
}

void ITAFFT::execute( )
{
	if( m_pRealization == NULL )
		ITA_EXCEPT1( MODAL_EXCEPTION, "Attempt to execute unplanned ITAFFT" );
	m_pRealization->execute( );
}

void ITAFFT::execute( float* in, float* out )
{
	if( m_pRealization == NULL )
		ITA_EXCEPT1( MODAL_EXCEPTION, "Attempt to execute unplanned ITAFFT" );
	m_pRealization->execute( in, out );
}

std::string ITAFFT::toString( )
{
	return ( m_pRealization == NULL ? std::string( "unplanned ITAFFT" ) : m_pRealization->toString( ) );
}
