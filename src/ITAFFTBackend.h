/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_FFT_BACKEND
#define INCLUDE_WATCHER_ITA_FFT_BACKEND

#include <string>

// Vorwärtsdeklarationen
class ITAFFTRealization;

// Diese abstrakte Klasse definiert die Schnittstelle für die Erzeugung
// von Realisierungen von FFTs, wie sie die Backends bereitstellen.

class ITAFFTBackend
{
public:
	virtual ~ITAFFTBackend( ) { };

	// Planungsmethode wie in der Klasse ITAFFT
	virtual ITAFFTRealization* plan( int type, int size, float* in, float* out, unsigned int uiFlags ) = 0;
};

#endif // INCLUDE_WATCHER_ITA_FFT_BACKEND
