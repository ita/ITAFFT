/*
 *  ITAFFT, eine Wrapper-Bibliothek f�r schnelle Fouriertransformationen
 *
 *  Autor: Frank Wefers (Frank.Wefers@akustik.rwth-aachen.de)
 *
 *  (c) Copyright 2008 Institut f�r Technische Akustik (ITA), RWTH Aachen
 *
 */

// $Id: StressTests.cpp,v 1.1 2008-11-06 21:36:12 fwefers Exp $

/*
 *  In diesem Modul werden Stress-Tests mit der ITAFFT durchgef�hrt.
 *  Der Test wurde entwickelt um Thread-safety Probleme mit der FFTW3
 *  nachzuspielen und deren Bugfixing zu �berpr�fen.
 */

#include <FastMath.h>
#include <ITAException.h>
#include <ITAFFT.h>
#include <ITAStringUtils.h>
#include <conio.h>
#include <cstdlib>
#include <ctime>
#include <process.h>
#include <stdio.h>
#include <windows.h>

void StressThreadProc( LPVOID lpParam )
{
	printf( "Thread %d is up and running!\n", *( (int*)lpParam ) );

	while( true )
	{
		int k = ( rand( ) % 16 ) + 1;
		int n = 1 << k;

		float* a = fm_falloc( n + 2 );
		float* b = fm_falloc( n + 2 );

		ITAFFT fft;
		fft.plan( rand( ) % 2 ? ITAFFT::FFT_R2C : ITAFFT::IFFT_C2R, n, a, b );

		fft.execute( );

		fm_free( a );
		fm_free( b );
	}
}

void test_Parallel_Plan_and_Exec( )
{
	srand( (unsigned)time( 0 ) );

	const int n = 20;
	HANDLE hThreads[n];
	int iThreadIDs[n];
	memset( hThreads, 0, sizeof( HANDLE ) );

	printf( "Running %d threads in parallel\n", n );
	for( int i = 0; i < n; i++ )
	{
		iThreadIDs[i] = i;
		hThreads[i]   = (HANDLE)_beginthread( StressThreadProc, 0, iThreadIDs + i );
	}

	printf( "\nStress test started. Press any key to abort!\n\n" );
	_getch( );

	for( int i = 0; i < n; i++ )
	{
		CloseHandle( hThreads[i] );
		printf( "Thread %d terminated.\n", i );
	}
	printf( "Done.\n" );
}

int main( int argc, char* argv[] )
{
	try
	{
		test_Parallel_Plan_and_Exec( );
	}
	catch( ITAException& e )
	{
		fprintf( stderr, "Error: %s\n", e.toString( ).c_str( ) );
	}

	return 0;
}