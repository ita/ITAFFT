/*
 *  ITAFFT, eine Wrapper-Bibliothek f�r schnelle Fouriertransformationen
 *
 *  Autor: Frank Wefers (Frank.Wefers@akustik.rwth-aachen.de)
 *
 *  (c) Copyright 2008 Institut f�r Technische Akustik (ITA), RWTH Aachen
 *
 */

// $Id: TryoutTests.cpp,v 1.2 2010-01-18 17:19:01 fwefers Exp $

#include <ITAException.h>
#include <ITAFFT.h>
#include <ITAStringUtils.h>
#include <stdio.h>

void test_FFT_R2C( )
{
	const int n = 8;
	float a[n];
	float b[n + 2];
	float c[n];

	for( int i = 0; i < n; i++ )
		a[i] = i;
	printf( "a = (%s)\n", FloatArrayToString( a, n, 3 ).c_str( ) );

	ITAFFT fft( ITAFFT::FFT_R2C, n, a, b );
	fft.execute( );

	printf( "b = (%s)\n", FloatArrayToString( b, n + 2, 3 ).c_str( ) );

	ITAFFT ifft( ITAFFT::IFFT_C2R, n, b, c );
	ifft.execute( );

	printf( "c = (%s)\n", FloatArrayToString( c, n, 3 ).c_str( ) );
}

void test_FFT_C2C( )
{
	const int n = 8;
	float a[2 * n];
	float b[2 * n];
	float c[2 * n];

	for( int i = 0; i < n; i++ )
	{
		a[2 * i]     = i;
		a[2 * i + 1] = 2 * i - 1;
	}

	printf( "a = (%s)\n", FloatArrayToString( a, 2 * n, 3 ).c_str( ) );

	ITAFFT fft( ITAFFT::FFT_C2C, n, a, b );
	fft.execute( );

	printf( "b = (%s)\n", FloatArrayToString( b, 2 * n, 3 ).c_str( ) );

	ITAFFT ifft( ITAFFT::IFFT_C2C, n, b, c );
	ifft.execute( );

	printf( "c = (%s)\n", FloatArrayToString( c, 2 * n, 3 ).c_str( ) );
}

void compare_SPLIT_FFT_R2C( )
{
	const int n = 8;
	float a[n + 2];
	float b[n + 2];
	float c[n + 2];

	printf( "Reference out-of-place FFT\n\n" );
	for( int i = 0; i < n; i++ )
		a[i] = i;
	printf( "a = (%s)\n", FloatArrayToString( a, n, 3 ).c_str( ) );

	ITAFFT fft( ITAFFT::FFT_R2C, n, a, b );
	fft.execute( );

	printf( "b = (%s)\n", FloatArrayToString( b, n + 2, 3 ).c_str( ) );
	printf( "a = (%s)\n\n", FloatArrayToString( a, n, 3 ).c_str( ) );


	printf( "Out-of-place Split-FFT\n\n" );
	ITAFFT sfft( ITAFFT::SPLIT_FFT_R2C, n, a, c );
	sfft.execute( );

	printf( "c = (%s)\n", FloatArrayToString( c, n + 2, 3 ).c_str( ) );

	// Testen ob das auch in-place geht:
	printf( "In-of-place Split-FFT\n\n" );
	ITAFFT sfft2( ITAFFT::SPLIT_FFT_R2C, n, a, a );
	sfft2.execute( );

	printf( "a = (%s)\n", FloatArrayToString( a, n + 2, 3 ).c_str( ) );

	// R�cktransformation (inplace)
	ITAFFT sifft2( ITAFFT::NORMALIZED_SPLIT_IFFT_C2R, n, a, a );
	sifft2.execute( );
	printf( "a = (%s)\n", FloatArrayToString( a, n, 3 ).c_str( ) );
}


int main( int argc, char* argv[] )
{
	try
	{
		// test_FFT_R2C();
		// test_FFT_C2C();
		compare_SPLIT_FFT_R2C( );
		/*
		        ITAFFT fft;
		        printf("%s\n", fft.toString().c_str());

		        // Versuchen ungeplante FFT auszuf�hren
		        //fft.execute();

		        fft.plan(ITAFFT::FFT_R2C, n, a, b);
		        bool f = fft.isPlanned();

		        printf("%s\n", fft.toString().c_str());

		        fft.execute();
		*/
	}
	catch( ITAException& e )
	{
		fprintf( stderr, "Error: %s\n", e.toString( ).c_str( ) );
	}

	return 0;
}
