/*
 *  ITAFFT, eine Wrapper-Bibliothek f�r schnelle Fouriertransformationen
 *
 *  Autor: Frank Wefers (Frank.Wefers@akustik.rwth-aachen.de)
 *
 *  (c) Copyright 2008 Institut f�r Technische Akustik (ITA), RWTH Aachen
 *
 */

// $Id: test.cpp,v 1.3 2008-04-23 10:01:20 fwefers Exp $

#include <ITAException.h>
#include <ITAFFT.h>
#include <ITASampleBuffer.h>
#include <ITAStringUtils.h>
#include <iostream>
#include <stdio.h>

void test_tobi( )
{
	int N = 2 << 18; // int( fSRTime * m_dSamplerate );

	ITASampleBuffer sbSRInputBuffer, sbSROutBuffer;
	ITASampleBuffer sbSRSpectrumBuffer;
	sbSRInputBuffer.Init( N, true );
	sbSROutBuffer.Init( N, true );
	sbSRSpectrumBuffer.Init( 2 * N, true );

	float* pfRealIn     = sbSRInputBuffer.GetData( );
	float* pfComplexOut = sbSRSpectrumBuffer.GetData( );
	float* pfRealOut    = sbSROutBuffer.GetData( );

	ITAFFT *m_pFFTEngine, *m_pIFFTEngine;
	m_pFFTEngine  = new ITAFFT( ITAFFT::FFT_R2C, N, pfRealIn, pfComplexOut, ITAFFT::PLAN_USING_ESTIMATION );
	m_pIFFTEngine = new ITAFFT( ITAFFT::IFFT_C2R, N, pfComplexOut, pfRealOut, ITAFFT::PLAN_USING_ESTIMATION );

	sbSRInputBuffer.GetData( )[0] = 0.89f;

	m_pFFTEngine->execute( );
	m_pIFFTEngine->execute( );

	sbSROutBuffer.mul_scalar( 1 / float( N ) );

	return;
}

void test_FFT_R2C( )
{
	const int n = 8;
	float a[n];
	float b[n + 2];
	float c[n];

	for( int i = 0; i < n; i++ )
		a[i] = float( i );
	printf( "a = (%s)\n", FloatArrayToString( a, n, 3 ).c_str( ) );

	ITAFFT fft( ITAFFT::FFT_R2C, n, a, b );
	fft.execute( );

	printf( "b = (%s)\n", FloatArrayToString( b, n + 2, 3 ).c_str( ) );

	ITAFFT ifft( ITAFFT::IFFT_C2R, n, b, c );
	ifft.execute( );

	printf( "c = (%s)\n", FloatArrayToString( c, n, 3 ).c_str( ) );
}

void test_FFT_C2C( )
{
	const int n = 8;
	float a[2 * n];
	float b[2 * n];
	float c[2 * n];

	for( int i = 0; i < n; i++ )
	{
		a[2 * i]     = float( i );
		a[2 * i + 1] = 2 * float( i ) - 1;
	}

	printf( "a = (%s)\n", FloatArrayToString( a, 2 * n, 3 ).c_str( ) );

	ITAFFT fft( ITAFFT::FFT_C2C, n, a, b );
	fft.execute( );

	printf( "b = (%s)\n", FloatArrayToString( b, 2 * n, 3 ).c_str( ) );

	ITAFFT ifft( ITAFFT::IFFT_C2C, n, b, c );
	ifft.execute( );

	printf( "c = (%s)\n", FloatArrayToString( c, 2 * n, 3 ).c_str( ) );
}

void test_SplitFFT_R2C( )
{
	const int n = 8;
	float a[n + 4];
	float b[n + 4];
	float c[n + 4];

	for( int i = 0; i < n; i++ )
		a[i] = float( i );
	for( int i = n; i < n + 4; i++ )
		a[i] = 0;
	printf( "a = (%s)\n", FloatArrayToString( a, n, 3 ).c_str( ) );

	ITAFFT fft( ITAFFT::SPLIT_FFT_R2C, n, a, b );
	fft.execute( );

	printf( "a = (%s)\n", FloatArrayToString( a, n, 3 ).c_str( ) );
	printf( "b = (%s)\n", FloatArrayToString( b, n + 2, 3 ).c_str( ) );

	ITAFFT ifft( ITAFFT::SPLIT_IFFT_C2R, n, b, c );
	ifft.execute( );

	printf( "c = (%s)\n\n\n", FloatArrayToString( c, n, 3 ).c_str( ) );

	printf( "a = (%s)\n", FloatArrayToString( a, n, 3 ).c_str( ) );
	ITAFFT fft2( ITAFFT::SPLIT_FFT_R2C, n, a, a );
	fft2.execute( );
	printf( "a = (%s)\n", FloatArrayToString( a, n + 2, 3 ).c_str( ) );

	ITAFFT ifft2( ITAFFT::SPLIT_IFFT_C2R, n, a, a );
	ifft2.execute( );

	printf( "a = (%s)\n", FloatArrayToString( a, n, 3 ).c_str( ) );
}

int main( int argc, char* argv[] )
{
	try
	{
		test_tobi( );

		// test_FFT_R2C();
		// test_FFT_C2C();
		// test_SplitFFT_R2C();
		/*
		        ITAFFT fft;
		        printf("%s\n", fft.toString().c_str());

		        // Versuchen ungeplante FFT auszuf�hren
		        //fft.execute();

		        fft.plan(ITAFFT::FFT_R2C, n, a, b);
		        bool f = fft.isPlanned();

		        printf("%s\n", fft.toString().c_str());

		        fft.execute();
		*/
	}
	catch( ITAException& e )
	{
		std::cout << e << std::endl;
	}

	return 0;
}